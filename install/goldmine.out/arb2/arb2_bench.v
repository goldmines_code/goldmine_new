`timescale 1ns/1ps

module arb2_bench;

reg req2;
reg req1;
reg rst;
reg clk;

wire gnt2;
wire gnt1;

arb2 arb2_ (
	.req2(req2),
	.req1(req1),
	.rst(rst),
	.clk(clk),
	.gnt2(gnt2),
	.gnt1(gnt1));

	initial begin
		$dumpfile("/data/lustre/Work/goldmine_old_tags/GoldMine-1.3.7/install/goldmine.out/arb2/arb2.vcd");
		$dumpvars(0, arb2_bench.arb2_);
		clk = 0;
		rst = 1;
		#26 rst = 0;
		#500000 $finish;
	end

	always begin
		#25 clk = ~clk;
	end

	always begin
		#24;
		req2 = $random;
		req1 = $random;
		#26;
	end
endmodule
