if (usbf_top_u1_u2_state==usbf_top_u1_u2_MEM_WR1)
True:	usbf_top.u1.u2.wr_last_en=usbf_top.u1.u2._rn192_wr_last_enif (usbf_top_u1_u2_rx_dma_en_r&&(!usbf_top_u1_u2_abort))
False:	usbf_top.u1.u2.next_state=usbf_top.u1.u2._rn184_next_state
usbf_top.u2.wreq=usbf_top.ma_req
usbf_top.u1.u2._rn205_mreq_d=1
usbf_top.u5.wb_ack_o=(!usbf_top_u5_wb_ack_o)
usbf_top.u5.wb_ack_s1a=usbf_top.u5.wb_ack_s1
usbf_top.rst=usbf_top.rst_i
usbf_top.u1.u2._rn201_mreq_d=1
usbf_top.u1.u2.mack=usbf_top.u1.mack
usbf_top.u1.u2.next_state=usbf_top.u1.u2._rn193_next_state
usbf_top.u1.u2._rn189_mreq_d=1
if (((usbf_top_u1_u0_pid_DATA&&usbf_top_u1_u0_rx_valid)&&usbf_top_u1_u0_rx_active)&&(!usbf_top_u1_u0_rx_err))
False:	usbf_top.u1.u0.data_valid_d=usbf_top.u1.u0._rn95_data_valid_d
usbf_top.u1.u2.next_state=usbf_top.u1.u2._rn203_next_state
usbf_top.u1.u2._rn183_wr_last_en=0
usbf_top.u1.u2.next_state=usbf_top.u1.u2._rn204_next_state
if (usbf_top_u1_u2_sizd_is_zero||usbf_top_u1_u2_abort)
True:	usbf_top.u1.u2.next_state=usbf_top.u1.u2._rn208_next_state
usbf_top.u1.u2.next_state=usbf_top.u1.u2._rn207_next_state
usbf_top.u1.u2.siz_dec=(usbf_top_u1_u2_rd_next&(usbf_top_u1_u2_sizd_c!=0))
usbf_top.u1.u2.adr_incw=((!usbf_top_u1_u2_dtmp_sel_r)&usbf_top_u1_u2_mack_r)
usbf_top.u1.u2.fill_buf0=((!usbf_top_u1_u2_adr_cw[0])&usbf_top_u1_u2_mack_r)
usbf_top.u1.u2.fill_buf1=(usbf_top_u1_u2_adr_cw[0]&usbf_top_u1_u2_mack_r)
usbf_top.u1.u2.mreq=((usbf_top_u1_u2_mreq_d&(!usbf_top_u1_u2_mack_r))|usbf_top_u1_u2_word_done_r)
usbf_top.u2._rn274_sram_we=(usbf_top_u2_wreq&usbf_top_u2_wwe)
usbf_top.u1.u2._rn203_next_state=usbf_top.u1.u2.IDLE
usbf_top.u1.u2._rn191_next_state=usbf_top.u1.u2.IDLE
usbf_top.u1.u2._rn206_next_state=usbf_top.u1.u2.IDLE
usbf_top.u1.u2._rn208_next_state=usbf_top.u1.u2.IDLE
if (!usbf_top_u1_u2_rst)
True:	usbf_top.u1.u2.state=usbf_top.u1.u2.IDLEusbf_top.u1.u2._rn200_next_state=usbf_top.u1.u2.IDLE
usbf_top.u1.u2._rn196_next_state=usbf_top.u1.u2.IDLE
usbf_top.u1.u2._rn186_next_state=usbf_top.u1.u2.IDLE
usbf_top.u1.u2._rn198_next_state=usbf_top.u1.u2.IDLE
if (!usbf_top_u1_u2_rst)
False:	usbf_top.u1.u2.state=usbf_top.u1.u2.next_stateusbf_top.ma_ack=usbf_top.u2.wack
usbf_top.u2.wsel=(!usbf_top_u2_mreq)
usbf_top.u1.u2.next_state=usbf_top.u1.u2._rn198_next_state
if (!usbf_top_u5_ma_ack)
False:	usbf_top.u5.next_state=usbf_top.u5._rn316_next_stateusbf_top.u2.wwe=usbf_top.ma_we
usbf_top.ma_we=usbf_top.u5.ma_we
usbf_top.u1.rx_data_valid=usbf_top.u1.u0.rx_data_valid
usbf_top.u1.u2.MEM_WR2=16
usbf_top.u1.u2.IDLE=1
usbf_top.wb_ack_o=usbf_top.u5.wb_ack_o
usbf_top.u1.mack=usbf_top.mack
usbf_top.u1.u2.mreq=((usbf_top_u1_u2_mreq_d&(!usbf_top_u1_u2_mack_r))|usbf_top_u1_u2_word_done_r)
usbf_top.u1.u2.rx_data_valid_r=usbf_top.u1.u2.rx_data_valid
usbf_top.u5.wb_ack_s1=usbf_top.u5.wb_ack_d
usbf_top.u1.u2.WAIT_MRD=2
usbf_top.u1.u2._rn192_wr_last_en=1
if (usbf_top_u1_u2_state==usbf_top_u1_u2_MEM_RD2)
True:	usbf_top.u1.u2.mreq_d=usbf_top.u1.u2._rn205_mreq_dusbf_top.u1.u2.rst=usbf_top.u1.rst
usbf_top.u1.u2.mreq=((usbf_top_u1_u2_mreq_d&(!usbf_top_u1_u2_mack_r))|usbf_top_u1_u2_word_done_r)
if (usbf_top_u1_u2_state==usbf_top_u1_u2_MEM_RD1)
True:	usbf_top.u1.u2.mreq_d=usbf_top.u1.u2._rn179_mreq_dusbf_top.u1.u2.MEM_RD3=128
usbf_top.u5.wb_ack_s2=usbf_top.u5.wb_ack_s1a
usbf_top.u5.ma_ack=usbf_top.ma_ack
usbf_top.u2.wack=(usbf_top_u2_wack_r&(!usbf_top_u2_mreq))
if (!usbf_top_u5_ma_ack)
True:	usbf_top.u5.ma_req=usbf_top.u5._rn317_ma_reqif (!usbf_top_u5_ma_ack)
True:	usbf_top.u5.ma_we=usbf_top.u5._rn314_ma_weif (((usbf_top_u1_u0_pid_DATA&&usbf_top_u1_u0_rx_valid)&&usbf_top_u1_u0_rx_active)&&(!usbf_top_u1_u0_rx_err))
False:	usbf_top.u1.u0.data_valid_d=usbf_top.u1.u0._rn105_data_valid_d
usbf_top.u1.u2._rn195_wr_last_en=1
if (!usbf_top_u5_rst)
False:	usbf_top.u5.state=usbf_top.u5.next_stateif (usbf_top_u1_u2_state==usbf_top_u1_u2_MEM_WR1)
True:	usbf_top.u1.u2.wr_last_en=usbf_top.u1.u2._rn183_wr_last_enif (usbf_top_u5_state==usbf_top_u5_W0)
True:	usbf_top.u5.wb_ack_d=usbf_top.u5._rn320_wb_ack_dusbf_top.sram_we_o=usbf_top.u2.sram_we
usbf_top.u1.mreq=usbf_top.u1.u2.mreq
usbf_top.u1.u2.next_state=usbf_top.u1.u2._rn206_next_state
usbf_top.u1.u0._rn105_data_valid_d=1
usbf_top.u2.wack_r=(!usbf_top_u2_wack)
usbf_top.u2.mack=usbf_top.u2.mreq
usbf_top.u1.u2.word_done_r=(usbf_top_u1_u2_word_done&(!usbf_top_u1_u2_word_done_r))
usbf_top.u1.u2._rn187_next_state=usbf_top.u1.u2.MEM_WR
usbf_top.u1.u2.MEM_RD1=32
usbf_top.u1.u2.next_state=usbf_top.u1.u2._rn196_next_state
if (usbf_top_u1_u2_state==usbf_top_u1_u2_MEM_RD1)
True:	usbf_top.u1.u2.mreq_d=usbf_top.u1.u2._rn201_mreq_dusbf_top.u1.u0._rn95_data_valid_d=0
if ((usbf_top_u1_u0_rx_valid&&usbf_top_u1_u0_rx_active)&&(!usbf_top_u1_u0_rx_err))
True:	usbf_top.u1.u0.data_valid_d=usbf_top.u1.u0._rn113_data_valid_d
usbf_top.u1.u2._rn193_next_state=usbf_top.u1.u2.MEM_WR1
usbf_top.u1.u2.next_state=usbf_top.u1.u2._rn200_next_state
usbf_top.u1.u2.next_state=usbf_top.u1.u2._rn187_next_state
usbf_top.u2.mreq=usbf_top.mreq
usbf_top.u1.u2.next_state=usbf_top.u1.u2._rn191_next_state
usbf_top.u2._rn275_sram_we=(usbf_top_u2_mwe&usbf_top_u2_mcyc)
usbf_top.u1.u2.mack_r=(usbf_top_u1_u2_mreq&usbf_top_u1_u2_mack)
usbf_top.u1.u2._rn179_mreq_d=0
usbf_top.u1.u2.next_state=usbf_top.u1.u2._rn186_next_state
usbf_top.u1.u2.MEM_RD2=64
usbf_top.u1.u2.MEM_WR=4
usbf_top.ma_req=usbf_top.u5.ma_req
usbf_top.u1.u2.MEM_WR1=8
usbf_top.mack=usbf_top.u2.mack
usbf_top.u2.mcyc=usbf_top.u2.mack
usbf_top.u1.u0.data_valid0=(usbf_top_u1_u0_rxv2&usbf_top_u1_u0_data_valid_d)
usbf_top.u1.u2.wr_last=(((usbf_top_u1_u2_adr_cb[1:0]!=0)&(!usbf_top_u1_u2_rx_data_valid_r))&usbf_top_u1_u2_wr_last_en)
if (usbf_top_u1_u2_state==usbf_top_u1_u2_MEM_WR1)
True:	usbf_top.u1.u2.wr_last_en=usbf_top.u1.u2._rn195_wr_last_enusbf_top.u1.u2.word_done=(((usbf_top_u1_u2_adr_cb[1:0]==3)&usbf_top_u1_u2_rx_data_valid_r)|usbf_top_u1_u2_wr_last)
usbf_top.u1.u2.next_state=usbf_top.u1.u2._rn197_next_state
usbf_top.u1.u2._rn207_next_state=usbf_top.u1.u2.MEM_RD3
usbf_top.u1.u2._rn204_next_state=usbf_top.u1.u2.MEM_RD2
usbf_top.u1.u2._rn209_next_state=usbf_top.u1.u2.MEM_RD2
usbf_top.u1.u2._rn185_next_state=usbf_top.u1.u2.MEM_RD1
if (usbf_top_u1_u2_rx_dma_en_r&&(!usbf_top_u1_u2_abort))
False:	usbf_top.u1.u2.next_state=usbf_top.u1.u2._rn178_next_state
if (usbf_top_u1_u2_adrb_is_3&&usbf_top_u1_u2_rd_next)
True:	usbf_top.u1.u2.next_state=usbf_top.u1.u2._rn209_next_state
usbf_top.u1.u2.rx_data_valid=usbf_top.u1.rx_data_valid
usbf_top.u2.sram_we=usbf_top.u2._rn274_sram_we
if (usbf_top_u1_u2_state==usbf_top_u1_u2_MEM_RD1)
True:	usbf_top.u1.u2.mreq_d=usbf_top.u1.u2._rn189_mreq_dif ((usbf_top_u1_u2_tx_dma_en_r&&(!usbf_top_u1_u2_abort))&&(!usbf_top_u1_u2_send_zero_length_r))
True:	usbf_top.u1.u2.next_state=usbf_top.u1.u2._rn185_next_state
usbf_top.u1.u2.wr_last=(((usbf_top_u1_u2_adr_cb[1:0]!=0)&(!usbf_top_u1_u2_rx_data_valid_r))&usbf_top_u1_u2_wr_last_en)
usbf_top.u1.u2._rn184_next_state=usbf_top.u1.u2.WAIT_MRD
usbf_top.u1.rst=usbf_top.rst
usbf_top.u1.u0.rx_data_valid=usbf_top.u1.u0.data_valid0
usbf_top.u1.u0._rn113_data_valid_d=1
usbf_top.u1.u2._rn197_next_state=usbf_top.u1.u2.MEM_WR2
usbf_top.mreq=usbf_top.u1.mreq
