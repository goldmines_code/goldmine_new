import os
import sys
from optparse import OptionParser
import networkx as nx
cwd = os.getcwd()
sys.path.append(cwd + "/../../.")
import verilog as vg
from parse_assertions import parse_assertion_file

def main():
    INFO = "Verilog code parser"
    optparser = OptionParser()
    optparser.add_option("-v","--version",action="store_true",dest="showversion",
                         default=False,help="Show the version")
    optparser.add_option("-c", "--config", action="append",dest="config_files",
                         default=[],help="Add the config file")
    optparser.add_option("-I","--include",dest="include",action="append",
                         default=[],help="Include path")
    optparser.add_option("-D",dest="define",action="append",
                         default=[],help="Macro Definition")
    optparser.add_option("-t","--top",dest="topmodule",
                         default="TOP",help="Top module, Default=TOP")
    optparser.add_option("--tree",action="store_true",dest="print_intermediate_trees",
                         default=False,help="This will print the possible trees")
    optparser.add_option("--nobind",action="store_true",dest="nobind",
                         default=False,help="No binding traversal, Default=False")
    optparser.add_option("--noreorder",action="store_true",dest="noreorder",
                         default=False,help="No reordering of binding dataflow, Default=False")
    (options, args) = optparser.parse_args()


    nsp = vg.NumericStringParser()

    ########### Using the verilog module ########################
    G = vg.parse_tree((options, args)) #parse the tree
    me = "example_i1"
    you = "example?i1"
    vg.display_graph(G, "full_graph") #dispay the tree
    assertions = parse_assertion_file() #using default filename "assertions.txt"
    for key in assertions: #loop through the assertions
        vg.walk_path_to_consequent(assertions[key], G)
        

if __name__ == '__main__':
    main()
