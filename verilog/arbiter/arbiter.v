module example(clk, rst, i1, i2, o1);

input clk, rst, i1;
input [1:0] i2;
output o1;
integer int_test;
reg r1, r2;

assign o1 = r1;

always @ (i1 or i2)
    case (i2)
        0: r2 = 1'b0;
        1: r2 = ~i1 | 1;
        2: r2 = ~i1;
        default: r2 = 1'b0;
    endcase

always @ (posedge clk)
    if (~rst)
        r1 <= 1'b1;
    else
        r1 <= ~r2;


endmodule

