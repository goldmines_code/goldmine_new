# GoldMine Re-Implementation Code in Python
# Authors: Debjit Pal ( dpal2@illinois.edu )
# Authors: Spencer Offenberger ( so10@illinois.edu )
# Owner:   Shobha Vasudevan ( shobhav@illinois.edu )

from system_sanity import check_system_sanity as css
css()
import sys, os
from argparse import ArgumentParser
from datetime import datetime as dt
from collections import OrderedDict as ODict
import pprint as pp
from decimal import Decimal


# Importing necessary helper functions
from helper import printTable, figlet_print, center_print, print_info, \
        print_newline, parse_cmdline_options, fatal_error, memory_usage, \
        goldmine_logger
# Importing Configuration file parsing function
from configuration import set_config_file_path, current_path, make_directory, change_directory, \
        summarize_report, remove_directory
# Importing Verilog parsing function
from verilog import parse_verilog, get_modules, get_top_modules, get_ports, getDefUseTarget, rank, \
        get_params
from static_analysis import CDFG, process_targets, plot_digraphs, plot_digraph,\
        get_targets_of_manual_assertions
# Importing Simulation function
from simulation import simulate, parse, write_csv, summary_report
#Importing mining function from miner, and analyze_manual_assertions
from assertion_miner.miner import miner, analyze_manual_assertions


if __name__ == "__main__":

    #css()

    parser = ArgumentParser()

    parser.add_argument("-a", "--aggregate", action="store_true",
                      help="Aggregate rankings for assertion importance, complexity, coverage and complexity",
                      dest="aggregate")
    parser.add_argument("-m", "--module", help="Top module of the design", dest="top", required=True)
    parser.add_argument("-c", "--clock", help="Clock signal", dest="clock", required=True)
    parser.add_argument("-r", "--reset", help="Reset signal", dest="reset", required=True)
    parser.add_argument("-p", "--parse", action="store_true", \
                      help="Parse the verilog file(s) and exit", dest="parse")
    parser.add_argument("-e", "--engine", help="Assertion mining engine", dest="engine", default='')
    parser.add_argument("-u", "--configuration_file_loc", help="GoldMine configuration file \
                      location", dest="config_loc", required=True)
    parser.add_argument("-v", "--vcd", help="VCD File(s)", dest="vcd", default='')
    parser.add_argument("-t", "--targets", help="Target variables seperated by comma \
                      for assertions mining", dest="targetv")
    parser.add_argument("-T", "--target_vectors", action="store_true", \
                      help="Target vector variables", default=False, dest="vectorf")
    parser.add_argument("-I", "--include", dest="include", action="append", \
                        default=[], help="Include Path")
    parser.add_argument("-V", "--verification", action="store_false", \
                      help="Specify to skip formal verification", dest="verif")
    parser.add_argument("-S", "--static_dump", action="store_true",\
                      help="Specify to dump static analysis info and graphics", dest="staticgraph")
    parser.add_argument("-M", "--manual_assertion", help="File containing user specified assertions", \
            dest="man_assertion_file", default='')

    # Specifying mutually exclusive command line options
    group = parser.add_mutually_exclusive_group()
    group.add_argument("-f", "--files", help="Location containing source Verilog files", dest="file_loc")
    group.add_argument("-F", "--file_list", help="A file containing name of verilog files with absolute path \
                        , one file in every line" , dest="lfile")

    Resource_Stat = ODict()

    work_directory_path = current_path() + '/goldmine.out'
    make_directory(work_directory_path)
    
    logname = work_directory_path + '/goldmine.log'
    glogger = goldmine_logger('goldmine', logname)

    args = parser.parse_args()
   
    figlet_print('GoldMine')
    
    # If no options are given, check the system sanity, print options and then exit
    if len(sys.argv) == 1:
        parser.print_help()
        exit(0)
    
    # Parse Command Line Options
    CMD_LINE_OPTIONS = parse_cmdline_options(args)
    # pp.pprint(CMD_LINE_OPTIONS)

    # Parse GoldMine Config File
    CONFIG = set_config_file_path(CMD_LINE_OPTIONS['CFG'], CMD_LINE_OPTIONS['VCD'] != '')
    # pp.pprint(CONFIG)

    center_print('Mining for design: ' + CMD_LINE_OPTIONS['TOP'])

    
    # Noting the time when the tool actually starts working, starting from simulation
    start_time = dt.now()
    
    figlet_print('V Parse')
    
    # PARSING DONE
    vparse_start_time = dt.now()
    
    ast, directives = parse_verilog(CMD_LINE_OPTIONS['VFILES'], CMD_LINE_OPTIONS['INCLUDE'], [])
    # GET MODULE NAMES
    Modules = {}
    get_modules(ast, Modules)
    if not Modules.keys():
        fatal_error('No module definition found in the source verilog file(s)')
     
    # CONFIRM TOP MODULE EXISTS IN THE AST
    Top_Modules = get_top_modules(Modules)
    top_module = ''
    if not Top_Modules:
        fatal_error('No top module found')
    elif CMD_LINE_OPTIONS['TOP'] in Top_Modules:
        top_module = CMD_LINE_OPTIONS['TOP']
    else:
        fatal_error('Specified top module not found in parse tree. Exiting..')

    # STORE CLOCKS, AND RESETS FROM COMMAND LINE SPECIFICATION. AUTOMATED CLOCK AND RESET IDENTIFICATION
    # NOT ENABLED AT THIS POINT OF TIME. THIS IS A TODO
    clks = CMD_LINE_OPTIONS['CLK']
    rsts = CMD_LINE_OPTIONS['RST']

    # CAPTURE THE MAXIMUM TEMPORAL LENGTH ALLOWED FOR AN ASSERTION
    temporal_depth = CONFIG['num_cycles']
    
    # Ports: Dictionary where Key := Port Type, Val := Corresponding port name
    Params = get_params(ast, top_module)
    Ports =  get_ports(ast, top_module, Params)
    def_vars, use_vars, targets = getDefUseTarget(Ports)

    #pp.pprint(Ports)

    # cones: A dictionary. Key is the name of the target variable and the Val is a directed graph
    #        containing the biunded cone of influence of the target bounded by the temporal_depth
    # PageRank: A dictionary. Key is the node name and the Val is the PageRank of the Variable used as
    #           the Global Importance
    figlet_print('C Ranking')

    if CMD_LINE_OPTIONS['TARGETS']:
        targets = CMD_LINE_OPTIONS['TARGETS']
    
    #print targets
    cones, PageRank, var_def_chain, var_use_chain, PathSets, dep_g, CDFGS \
            = CDFG(ast, temporal_depth, targets, clks, Params) 
    #for key in cones.keys():
    #    print 'Target: ' + key + ' :' + ' ' + str(cones[key].nodes()) + '\n'
    # Process Targets (bit blasts all vector signals in the cone)
    target_cones = process_targets(cones, Ports, targets, CMD_LINE_OPTIONS['VTARGETS'])
    
    
    vparse_end_time = dt.now()
    
    ######################
    mem_usage = memory_usage(os.getpid())
    print_info('Total time for parsing, ranking: ' + str(vparse_end_time - vparse_start_time))
    print_info('Peak Memory Usage for parsing, ranking: ' + str(round(Decimal(mem_usage / 1048576), 2)) + ' MB')
    Resource_Stat['V Parse & Ranking'] = [str(vparse_end_time - vparse_start_time), \
            str(round(Decimal(mem_usage / 1048576), 2))]
    ######################

    change_directory(work_directory_path)

    #PageRank, graph, var_def_chain, var_use_chain = rank(ast, def_vars, use_vars)

    figlet_print('Data Gen')
    make_directory(top_module)
    change_directory(top_module)
    
    if CMD_LINE_OPTIONS['STATICGRAPH']:
        # Dump all static analysis reports in the static directory
        static_dir = current_path() + '/static'
        make_directory(static_dir)

        dep_file = current_path() + '/static/' + top_module + '.dep'
        dep_handle = open(dep_file, 'w')
        pp.pprint(dep_g.edges(data='weight'), dep_handle)
        dep_handle.close()

        #dep_g_file_name = current_path() + '/static/graphs/' + top_module + '.dot'
        vdg_dir_name =  current_path() + '/static/var_dep_graph'
        remove_directory(vdg_dir_name)
        make_directory(vdg_dir_name)
        plot_digraph([dep_g], current_path(), [top_module], 'var_dep_graph')
    
        plot_digraphs(CDFGS, 'cdfg')
        
        cone_dir_name = current_path() + '/static/cone'
        remove_directory(cone_dir_name)
        make_directory(cone_dir_name)
        for key in cones.keys():
            plot_digraph([cones[key]], current_path(), [key], 'cone')
    
        def_file = current_path() + '/static/' + top_module + '.def'
        def_handle = open(def_file, 'w')
        pp.pprint(var_def_chain, def_handle)
        def_handle.close()
 
        use_file = current_path() + '/static/' + top_module + '.use'
        use_handle = open(use_file, 'w')
        pp.pprint(var_use_chain, use_handle)
        use_handle.close()

        path_file = current_path() + '/static/' + top_module + '.path'
        path_handle = open(path_file, 'w')
        pp.pprint(PathSets, path_handle)
        path_handle.close()
   
        PageRankO =  ODict(sorted(PageRank.items(), key=lambda t: t[1], reverse=True))
        rank_file = current_path() + '/static/' + top_module + '.rank'
        content = printTable(PageRankO, ['Variable', 'Rank']) 
        rfile = open(rank_file, 'w')
        rfile.write(content)
        rfile.close()
    

    vcd_file_path = CMD_LINE_OPTIONS['VCD'] if CMD_LINE_OPTIONS['VCD'] \
            else current_path() + '/' + top_module + '.vcd'
   

    if CMD_LINE_OPTIONS['MAN_ASSERTIONS']:
        # Assertion_dict is a dictionary where the key is the target of the specified assertions
        # and the value is a list containing all the assertions specified for that target variable
        mtargets = get_targets_of_manual_assertions(CMD_LINE_OPTIONS['MAN_ASSERTIONS'])
        targets = mtargets.keys()
        for target in targets:
            analyze_manual_assertions(target_cones[target], target, CONFIG, top_module, clks, rsts, \
                CMD_LINE_OPTIONS['VFILES'], CMD_LINE_OPTIONS['INCLUDE'], cones[target], \
                CMD_LINE_OPTIONS['AGGREGATE'], CMD_LINE_OPTIONS['VERIF'], mtargets[target], 'manual')
    
        end_time = dt.now()
        print_newline()
        print_info('Total Run Time: ' + str(end_time - start_time))
        exit(0)
    
    simulation_start_time = dt.now()

    if not CMD_LINE_OPTIONS['VCD']:
        # Iverilog get precedence over VCS
        if CONFIG['iverilog']:
            simulate(top_module, clks, rsts, CMD_LINE_OPTIONS['VFILES'], \
                    CMD_LINE_OPTIONS['INCLUDE'], CONFIG['max_sim_cycles'], 'iverilog', CONFIG, Ports)
        else:
            simulate(top_module, clks, rsts, CMD_LINE_OPTIONS['VFILES'], \
                    CMD_LINE_OPTIONS['INCLUDE'], CONFIG['max_sim_cycles'], 'vcs', CONFIG, Ports)
    
    simulation_end_time = dt.now()
    mem_usage = memory_usage(os.getpid())
    Resource_Stat['Simulation'] = [str(simulation_end_time - simulation_start_time), \
            str(round(Decimal(mem_usage / 1048576), 2))]
    
    # Either via Simulation or via Supplied VCD we have trace. Now its time to parse it 
    figlet_print('D Parse')
    vcdparse_start_time = dt.now() 
    # rows_ will be a Pandas DataFrame which has all the variables and its time stampped values upto
    # temporal_depth
    rows_, num_rows_, rows_invalid_type = parse(vcd_file_path, top_module, clks, temporal_depth, Ports)
    vcdparse_end_time = dt.now()
    mem_usage = memory_usage(os.getpid())
    
    summary_report(rows_invalid_type)
    print_info('Total time to parse VCD file: ' + str(vcdparse_end_time - vcdparse_start_time))
    Resource_Stat['D Parse'] = [str(vcdparse_end_time - vcdparse_start_time), \
            str(round(Decimal(mem_usage / 1048576), 2))]
    write_csv(rows_, top_module, '')

    # Time To Mine
    figlet_print('Mining')
    
    #tf = {'gnt1':['req1','req2','state','[1]req1','[1]req2','[1]state'],
    #      'gnt2':['req1','req2','state','[1]req1','[1]req2','[1]state']
    #      }

    targets = target_cones.keys()
    
    engine = CMD_LINE_OPTIONS['MENGINE'] if CMD_LINE_OPTIONS['MENGINE'] else CONFIG['engine']
    
    mined_assertion = []

    print_info('Engine used for mining: ' + engine)

    mine_start_time = dt.now()
    
    for target in targets:
        print_newline()
        center_print('Mining for target :--> ' + target)
        miner(target_cones[target], target, rows_, rows_invalid_type, CONFIG, top_module, \
                    clks, rsts, CMD_LINE_OPTIONS['VFILES'], CMD_LINE_OPTIONS['INCLUDE'], \
                    cones[target], CMD_LINE_OPTIONS['AGGREGATE'], CMD_LINE_OPTIONS['VERIF'], engine)
    # Time to Verify
    mine_end_time = dt.now()
    mem_usage = memory_usage(os.getpid())

    print_info('Total time to mine: ' + str(mine_end_time - mine_start_time))
    Resource_Stat['Mining'] = [str(mine_end_time - mine_start_time), \
            str(round(Decimal(mem_usage / 1048576), 2))]

    # Noting the time when the tool actually stops working, ending at dumping all results
    end_time = dt.now()
    mem_usage = memory_usage(os.getpid())

    print_newline()
    print_info('Total Run Time: ' + str(end_time - start_time))

    Resource_Stat['Overall'] = [str(end_time - start_time), str(round(Decimal(mem_usage / 1048576), 2))]

    summarize_report(top_module, targets, Resource_Stat, engine)
