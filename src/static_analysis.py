# GoldMine Re-Implementation Code in Python
# Authors: Debjit Pal ( dpal2@illinois.edu )
# Authors: Spencer Offenberger ( so10@illinois.edu )
# Owner:   Shobha Vasudevan ( shobhav@illinois.edu )

import networkx as nx
from networkx.drawing.nx_agraph import *
import numpy as np
import pprint as pp
import itertools as itools
import regex as re
import multiprocessing as mps
import pygraphviz as pgv

NUMBER_OF_PROCESSES = mps.cpu_count()

from verilog import get_rhs_cond_nodes, get_comp_nodes, get_lvalue, get_rhs_constants, analyze_pagerank, \
        get_ports
from configuration import current_path, remove_directory, make_directory
from helper import exec_command, print_warning
from formal_verifier import worker

#### This Node Type Dictionary is for the Graph
Type = {'IfStatement': 'IF',
        'CaseStatement': 'CS',
        'CasexStatement': 'CX',
        'Case': 'CA',
        'Block': 'BL',
        'NonblockingSubstitution': 'NS',
        'BlockingSubstitution': 'BS',
        'Assign': 'AS',
        'Always': 'AL'
        }

def get_port_type(module_name, mod_inst_chains):

    mports = [] 
    mtyps = []

    for mod_inst_chain in mod_inst_chains:
        root_node = get_root_node(mod_inst_chain)[0]
        if root_node == module_name:
            mports = mod_inst_chain.node[root_node]['port']
            mtyps = mod_inst_chain.node[root_node]['typ']
            break

    return mports, mtyps

def construct_Mod_Inst_Chain(ast, mod_inst_chains):

    if (ast.__class__.__name__ == 'ModuleDef'):
        ModuleGraph = nx.DiGraph()

        port = []
        typ = []

        node_name = ast.name
        
        ports = get_ports(ast, node_name)

        for key in ports.keys():
            port.extend(ports[key])
            if key == 'IPort':
                typ_ = ['inp'] * len(ports[key])
                typ.extend(typ_)
                del typ_
            elif key == 'OPort':
                typ_ = ['oup'] * len(ports[key])
                typ.extend(typ_)
                del typ_
    
        print node_name
        #print port
        #print typ
        
        ModuleGraph.add_node(node_name, ast=ast, \
                                        port=port, \
                                        typ=typ)

        mod_inst_chains.append(ModuleGraph)

    for c in ast.children():
        construct_Mod_Inst_Chain(c, mod_inst_chains)

    return

        
def add_nodes_Mod_Inst_Chain(ast, mod_inst_chain):
    
    NodeQueue = mod_inst_chain.nodes()
    top_node = NodeQueue[0]

    while NodeQueue:
        p_node_name = NodeQueue.pop()
        p_node_ast = mod_inst_chain.node[p_node_name]['ast']
        if (p_node_ast.__class__.__name__ == 'Instance'):
            portname = []
            argname = []
            
            inst_node_name = p_node_ast.name
            inst_module_name = p_node_ast.module
            print 'Instance name: ' + inst_node_name

            portlist = p_node_ast.portlist
            
            for port in portlist:
                portname_ = port.portname
                argname_ = []
                get_rhs_cond_nodes(port.argname, argname_)
                portname.append(portname_)
                argname.append(argname_)
            
            mod_inst_chain.add_node(inst_node_name, module_name=inst_module_name,\
                                                    portname=portname, \
                                                    argname=argname)
            mod_inst_chain.add_edge(top_node, inst_node_name)
            
            #print inst_node_name
            #print portname
            #print argname
            #print '\n' * 2

        for c in p_node_ast.children():
            try:
                node_name = str(c.lineno) + ':' + c.name
            except AttributeError:
                node_name = str(c.lineno) + ':' + c.__class__.__name__
            NodeQueue.append(node_name)

            mod_inst_chain.add_node(node_name, ast=c)
    
    nodes = mod_inst_chain.nodes()
    # Cleanup all the nodes that are not related via Module_Instantiation relationship
    for node in nodes:
        if mod_inst_chain.in_degree(node) == 0 and \
           mod_inst_chain.out_degree(node) == 0 and \
           node != top_node:
            mod_inst_chain.remove_node(node)
    
    #print 'Final nodes: ' + str(mod_inst_chain.nodes())
    return mod_inst_chain


def construct_CDFG(ast, CDFGS, clks, indent):
    # ast: returned by the PyVerilog Parser after parsing the Verilog Files(s)
    
    if (ast.__class__.__name__ == 'Always'):
        ALGraph = nx.DiGraph()
        typ = ast.__class__.__name__
        node_name = str(ast.lineno) + ':' + Type[typ]

        sens_list = []
        get_rhs_cond_nodes(ast.sens_list, sens_list)
        clk_name = clks.keys()[0]
        
        ALGraph.add_node(node_name, typ=typ, \
                                    sens=sens_list, \
                                    clk_sens=True if clk_name in sens_list else False, \
                                    statements=[], \
                                    ast=ast)
        CDFGS.append(ALGraph)

    elif (ast.__class__.__name__ == 'Assign'):
        ASGraph = nx.DiGraph()
        typ = ast.__class__.__name__
        node_name = str(ast.lineno) + ':' + Type[typ]
        
        ASGraph.add_node(node_name, typ='Assign', \
                                    statements=[], \
                                    ast=ast)
        CDFGS.append(ASGraph)
    
    for c in ast.children():
        #print '-' * indent + ': ' + c.__class__.__name__ + ' ' + str(c.lineno)
        construct_CDFG(c, CDFGS, clks, indent + 2)
    

    return

def add_nodes_to_CDFG(ast, CDFG):
    
    NodeQueue = CDFG.nodes()
    
    while NodeQueue:
        p_node_name = NodeQueue.pop()
        p_node_ast = CDFG.node[p_node_name]['ast']
        if (p_node_ast.__class__.__name__ == 'Always'):
            '''
            for c in p_node_ast.children():
                if (c.__class__.__name__ == 'IfStatement'):
                    c_node_name = str(c.lineno)
                    NodeQueue.append(c_node_name)
                    CDFG.add_node(c_node_name, typ='IfStatement', \
                                               statements=[], \
                                               ast=c)
                    CDFG.add_edge(p_node_name, c_node_name, cond=[])

                elif (c.__class__.__name__ == 'BlockingSubstitution' or
                      c.__class__.__name__ == 'NonblockingSubstitution'):
                    CDFG.node[p_node_name]['statements'].append(c)

                elif (c.__class__.__name__ == 'CaseStatement' or
                      c.__class__.__name__ == 'CaseXStatement'):
                    c_node_name = str(c.lineno)
                    NodeQueue.append(c_node_name)
                    CDFG.add_node(c_node_name, typ='CaseStatement', \
                                               statements=[], \
                                               ast=c)
                    CDFG.add_edge(p_node_name, c_node_name, cond=[])
                # Consider Block
            '''
            stmt_ast = p_node_ast.statement
            typ = stmt_ast.__class__.__name__
            stmt_node_name = str(stmt_ast.lineno) + ':' + Type[typ]

            #print 'Always child name: ' + stmt_node_name

            NodeQueue.append(stmt_node_name)
            CDFG.add_node(stmt_node_name, typ=typ, \
                                          statements=[], \
                                          ast=stmt_ast)
            CDFG.add_edge(p_node_name, stmt_node_name, cond=[], \
                                                       lineno=None)

        elif (p_node_ast.__class__.__name__ == 'IfStatement'):
            condition = []
            get_rhs_cond_nodes(p_node_ast.cond, condition)
            
            true_ast = p_node_ast.true_statement
            true_typ = true_ast.__class__.__name__
            
            false_ast = p_node_ast.false_statement

            true_node_name = str(true_ast.lineno) + ':' + Type[true_typ]
            #print 'IfStatement true child name: ' + true_node_name

            try:
                false_typ = false_ast.__class__.__name__
                false_node_name = str(false_ast.lineno) + ':' + Type[false_typ]
                #print 'IfStatement false child name: ' + false_node_name
            except AttributeError:
                false_node_name = ''

            NodeQueue.append(true_node_name)
            CDFG.add_node(true_node_name, typ=true_typ, \
                                          statements=[], \
                                          ast=true_ast)
            CDFG.add_edge(p_node_name, true_node_name, cond=condition, \
                                                       lineno=p_node_ast.lineno)

            if false_node_name:
                NodeQueue.append(false_node_name)
                CDFG.add_node(false_node_name, typ=false_typ, \
                                               statements=[], \
                                               ast=false_ast)
                CDFG.add_edge(p_node_name, false_node_name, cond=condition, \
                                                            lineno=p_node_ast.lineno)
    

        elif (p_node_ast.__class__.__name__ == 'CaseStatement' or
              p_node_ast.__class__.__name__ == 'CasexStatement'):
            comp = []
            get_comp_nodes(p_node_ast.comp, comp)
            caselist_ast = p_node_ast.caselist

            for case_ast in caselist_ast:
                case_typ = case_ast.__class__.__name__
                case_node_name = str(case_ast.lineno) + ':' + Type[case_typ]
                #print 'CaseStatement/CaseXStatement true child name: ' + case_node_name
                case_statement_ast = case_ast.statement

                NodeQueue.append(case_node_name)
                CDFG.add_node(case_node_name, typ=case_typ, \
                                              statements=[], \
                                              ast=case_ast)
                CDFG.add_edge(p_node_name, case_node_name, cond=comp, \
                                                           lineno=p_node_ast.lineno)

        elif (p_node_ast.__class__.__name__ == 'Block'):
            stmts = p_node_ast.statements
            for c in stmts:
                if (c.__class__.__name__ == 'IfStatement'):
                    c_typ = c.__class__.__name__
                    c_node_name = str(c.lineno) + ':' + Type[c_typ]
                    #print 'Block-IfStatement child name: ' + c_node_name
                    NodeQueue.append(c_node_name)
                    CDFG.add_node(c_node_name, typ=c_typ, \
                                               statements=[], \
                                               ast=c)
                    CDFG.add_edge(p_node_name, c_node_name, cond=[], \
                                                            lineno=None)

                elif (c.__class__.__name__ == 'BlockingSubstitution' or
                      c.__class__.__name__ == 'NonblockingSubstitution'):
                    CDFG.node[p_node_name]['statements'].append(c)

                elif (c.__class__.__name__ == 'CaseStatement' or
                      c.__class__.__name__ == 'CasexStatement'):
                    c_typ = c.__class__.__name__
                    c_node_name = str(c.lineno) + ':' + Type[c_typ]
                    #print 'Block-CaseStatement/CaseXStatement child name: ' + c_node_name
                    NodeQueue.append(c_node_name)
                    CDFG.add_node(c_node_name, typ=c_typ, \
                                               statements=[], \
                                               ast=c)
                    CDFG.add_edge(p_node_name, c_node_name, cond=[], \
                                                            lineno=None)
        
        elif (p_node_ast.__class__.__name__ == 'Case'):
            stmt_ast = p_node_ast.statement
            stmt_typ = stmt_ast.__class__.__name__
            stmt_node_name = str(stmt_ast.lineno) + ':' + Type[stmt_typ]
            #print 'Case child name: ' + stmt_node_name

            NodeQueue.append(stmt_node_name)
            CDFG.add_node(stmt_node_name, typ=stmt_typ, \
                                          statements=[], \
                                          ast=stmt_ast)
            CDFG.add_edge(p_node_name, stmt_node_name, cond=[], \
                                                       lineno=None)

        elif (p_node_ast.__class__.__name__ == 'NonblockingSubstitution' or
              p_node_ast.__class__.__name__ == 'BlockingSubstitution'):  
            CDFG.node[p_node_name]['statements'].append(p_node_ast)
    
    root_node = get_root_node(CDFG)[0]
    leaf_nodes = get_leaf_nodes(CDFG)
    
    #print root_node
    #print leaf_nodes

    joint_node = 'Leaf_' + str(root_node)
    CDFG.add_node(joint_node)
    for lnode in leaf_nodes:
        CDFG.add_edge(lnode, joint_node, cond=[], \
                                         lineno=None)

    return

def traverse_CDFG(CDFG):
    # Pre-Order Traversal
    root_node = get_root_node(CDFG)[0]
    leaf_node = get_leaf_nodes(CDFG)[0]

    nodes = CDFG.nodes()
    
    # It is a dictionary of lists. Each Key is a destination node and the lists are different paths from
    # root node to destination node
    # unique_paths_in_CDFG = {}

    #for dst_node in leaf_nodes:
    #    paths = all_unique_paths(CDFG, root_node, dst_node)
    #    unique_paths_in_CDFG[dst_node] = paths
    #    del paths

    unique_paths_in_CDFG = all_unique_paths(CDFG, root_node, leaf_node)

    return unique_paths_in_CDFG

def all_unique_paths(CDFG, src, dst):

    nodes = CDFG.nodes()
    visited = [False] * len(nodes)
    
    # List of nodes of one path from src --> dst
    path = []
    # List of all paths from src --> dst
    paths = []

    s = nodes.index(src)
    d = nodes.index(dst)

    all_unique_path_dfs(CDFG, nodes, s, d, visited, path, paths)
    
    del path 

    return paths

def all_unique_path_dfs(CDFG, nodes, u, d, visited, path, paths):
    
    visited[u] = True
    path.append(nodes[u])

    if u == d:
        # Be careful. Since we are popping path later, do slicing to make a true copy. Else
        # path will be all empty
        paths.append(path[:])
    else:
        for i in CDFG.neighbors(nodes[u]):
            if visited[nodes.index(i)] == False:
                all_unique_path_dfs(CDFG, nodes, nodes.index(i), d, visited, path, paths)

    path.pop()
    visited[u] = False

    return

def get_root_node(CDFG):
    root_node_index = [n for n, d in CDFG.in_degree().items() if d == 0]
    return root_node_index

def get_leaf_nodes(CDFG):
    leaf_node_indices = [n for n, d in CDFG.out_degree().items() if d == 0]
    return leaf_node_indices

def plot_digraph(digraphs, curr_path, root, object_type):
    for digraph in digraphs:
        if not root:
            root_node = get_root_node(digraph)
            root_node[0] = root_node[0].replace(':', '_')
        else:
            root_node = root
        file_name = curr_path + '/static/' + object_type + '/' + root_node[0] + '.dot'
        A = to_agraph(digraph)
        A.layout()
        A.draw(file_name)
        pdf_file_name = file_name[:file_name.rfind('.')] + '.pdf'
        dot_command = 'dot -Tpdf ' + file_name + ' -o ' + pdf_file_name + ' && rm -rfv ' + file_name
        #print dot_command
        exec_command(dot_command, 'PIPE', 'PIPE')
    return


def plot_digraphs(CDFGS, object_type):
    curr_path = current_path()
    dir_name = curr_path + '/static/' + object_type
    remove_directory(dir_name)
    make_directory(dir_name)

    if not len(CDFGS):
        print_warning('No CDFG found to draw')
        return
    cdfg_per_core = []

    task_queue = mps.Queue()
    done_queue = mps.Queue()

    i = 0
    for id in range(NUMBER_OF_PROCESSES):
        cdfg_per_core.append([])
        
    for CDFG in CDFGS:
        cdfg_per_core[i % NUMBER_OF_PROCESSES].append(CDFG)
        i = i + 1
        if  i == NUMBER_OF_PROCESSES:
            i = 0

    if len(cdfg_per_core) > 0:
        TASKS = [(plot_digraph, (cdfg_per_core[i], curr_path, [], object_type)) \
                for i in range(NUMBER_OF_PROCESSES) if cdfg_per_core[i]]
        #root_node = get_root_node(CDFG)
        #file_name = curr_path + '/static/graphs/' + root_node[0] + '.dot'
        #plot_digraph(CDFG, file_name)
    for task in TASKS:
        task_queue.put(task)

    for i in range(len(TASKS)):
        mps.Process(target=worker, args=(task_queue, done_queue)).start()
    
    for i in range(len(TASKS)):
        done_queue.get()

    for i in range(len(TASKS)):
        task_queue.put('STOP')

    return

def CDFG(ast, temp_length_max, targets, clks, Params):
    # List containing Control Data Flow Graph of each of the Always block
    # and Assign Block
    CDFGS = []
    mod_inst_chains = []
    
    # Construction of CDFG per procedural block
    construct_CDFG(ast, CDFGS, clks, 2)
    for CDFG in CDFGS:
        cdfg_type = CDFG.node[CDFG.nodes()[0]]['typ']
        if cdfg_type == 'Always':
            cdfg_ast =  CDFG.node[CDFG.nodes()[0]]['ast']
            add_nodes_to_CDFG(cdfg_ast, CDFG)
    # Traversal of CDFG per procedural block
    PathSets = []
    for CDFG in CDFGS:
        cdfg_type = CDFG.node[get_root_node(CDFG)[0]]['typ']
        if cdfg_type == 'Always':
            PathSets.append(traverse_CDFG(CDFG))
  
    #pp.pprint(PathSets)
    
    '''
    construct_Mod_Inst_Chain(ast, mod_inst_chains)

    for mod_inst_chain in mod_inst_chains:
        mod_inst_ast = mod_inst_chain.node[mod_inst_chain.nodes()[0]]['ast']
        add_nodes_Mod_Inst_Chain(mod_inst_ast, mod_inst_chain)
        #print mod_inst_chain.nodes()
    '''

    var_def_chain = get_var_def_chain(CDFGS, PathSets, Params)
    #pp.pprint(var_def_chain)
    var_use_chain = get_var_use_chain(CDFGS, PathSets, Params)
    #pp.pprint(var_use_chain)
    dep_g = construct_var_dep_graph(var_def_chain, var_use_chain, mod_inst_chains)
    PageRank = analyze_pagerank(dep_g)
    
    # Since Importance and Complexity of each variable is independent of the assertions and solely depend
    # on the design, we calculate the Importance, Complexity, and Rank of each node here during static analysis
    # phase of the analysis. 
    # When we have the assertions we just add up the importance and complexities of the respective nodes from
    # the COI graph

    # targets = ['gnt1']

    # cones will be a dictionary where the Key of the Dictionary is the Target variable,
    # and the Val of the Key is the bounded COI graph for that target variable
    cones = cone_of_influence(targets, var_def_chain, var_use_chain, \
                            dep_g, PageRank, temp_length_max)
    ''' 
    for key in cones.keys():
        cone = cones[key]
        print nx.get_node_attributes(cone, 'importance')
        print nx.get_node_attributes(cone, 'complexity')
        print nx.get_node_attributes(cone, 'rank')
        print '\n' * 2
    '''

    return cones, PageRank, var_def_chain, var_use_chain, PathSets, dep_g, CDFGS

def get_var_use_chain(CDFGS, PathSets, Params):

    var_use_chain = {}

    path_index = 0

    for idx1 in range(len(CDFGS)):
        try:
            PathSet = PathSets[path_index]
        except:
            PathSet = []

        CDFG = CDFGS[idx1]

        path_key_set = set(itools.chain(*PathSet))
        CDFG_key_set = set(CDFG.nodes())

        if list(set.intersection(path_key_set, CDFG_key_set)):
            path_index = path_index + 1

        cdfg_type = CDFG.node[get_root_node(CDFG)[0]]['typ']
        if cdfg_type == 'Assign':
            ast = CDFG.node[CDFG.nodes()[0]]['ast']
            def_var = get_lvalue(ast.left)
            use_vars = []
            if not get_rhs_constants(ast.right):
                get_rhs_cond_nodes(ast.right, use_vars)
                for use_var in use_vars:
                    if use_var in Params.keys():
                        use_vars.remove(use_var)
            if use_vars:
                for use_var in use_vars:
                    if use_var not in var_use_chain.keys():
                        var_use_chain[use_var] = {'Sensitivities': [ast],
                                                  'Lines': [str(ast.lineno) + ':D'],
                                                  'DefVars': [def_var]
                                                  }
                    else:
                        var_use_chain[use_var]['Sensitivities'].append(ast)
                        var_use_chain[use_var]['Lines'].append(str(ast.lineno) + ':D')
                        var_use_chain[use_var]['DefVars'].append(def_var)
        elif cdfg_type == 'Always':
            for Path in PathSet:
                cond = []
                for idx2 in range(1, len(Path)):
                    edge = (Path[idx2 - 1], Path[idx2])
                    statements = CDFG.node[Path[idx2 - 1]]['statements']
                    if statements:
                        for statement in statements:
                            def_var = get_lvalue(statement.left)
                            use_vars = []
                            if not get_rhs_constants(statement.right):
                                get_rhs_cond_nodes(statement.right, use_vars)
                                for use_var in use_vars:
                                    if use_var in Params.keys():
                                        use_vars.remove(use_var)

                            for use_var in use_vars:
                                if use_var not in var_use_chain.keys():
                                    var_use_chain[use_var] = {'Sensitivities': [statement],
                                                              'Lines': [str(statement.lineno) + ':D'],
                                                              'DefVars': [def_var]
                                                              }
                                else:
                                    var_use_chain[use_var]['Sensitivities'].append(statement)
                                    var_use_chain[use_var]['Lines'].append(str(statement.lineno) + ':D')
                                    var_use_chain[use_var]['DefVars'].append(def_var)
                            del use_vars

                    cond.append(tuple([CDFG.get_edge_data(*edge)['cond'], \
                                       CDFG.get_edge_data(*edge)['lineno']]))

                    #cond.append(CDFG.get_edge_data(*edge)['cond'])

                for cond_ in cond:
                    if cond_[0]:
                        for use_var in cond_[0]:
                            if use_var not in var_use_chain.keys():
                                var_use_chain[use_var] = {'Sensitivities': [cond_[0]],
                                                          'Lines': [str(cond_[1]) + ':C' if cond_[1] else \
                                                                  cond_[1]],
                                                          'DefVars': [None]
                                                          }
                            else:
                                var_use_chain[use_var]['Sensitivities'].append(cond_[0])
                                var_use_chain[use_var]['Lines'].append(str(cond_[1]) + ':C' if cond_[1] else \
                                        cond_[1])
                                var_use_chain[use_var]['DefVars'].append(None)
                del cond

    #pp.pprint(var_use_chain)

    return var_use_chain


def get_var_def_chain(CDFGS, PathSets, Params):

    var_def_chain = {}

    path_index = 0
    
    #print 'Length of CDFG: ' + str(len(CDFGS))
    #print 'Length of PathSets: ' + str(len(PathSets))

    for idx1 in range(len(CDFGS)):
        try:
            PathSet = PathSets[path_index]
        except IndexError:
            PathSet = []

        CDFG = CDFGS[idx1]
        
        path_key_set = set(itools.chain(*PathSet))
        CDFG_key_set = set(CDFG.nodes())

        if list(set.intersection(path_key_set, CDFG_key_set)):
            path_index = path_index + 1
        
        cdfg_type = CDFG.node[get_root_node(CDFG)[0]]['typ']
        if cdfg_type == 'Always':
            #for key in PathSet.keys():
            #    Paths = PathSet[key]
            #    print Paths
            clocked = CDFG.node[get_root_node(CDFG)[0]]['clk_sens']
            for Path in PathSet:
                #print Path
                cond = []
                clines = []
                for idx2 in range(1, len(Path)):
                    edge = (Path[idx2 - 1], Path[idx2])
                    #print edge
                    statements = CDFG.node[Path[idx2 - 1]]['statements']
                    #print statements
                    if statements:
                        #c_dep = list(itools.chain(*cond))
                        c_dep = cond[:]
                        c_lines = clines[:]
                        for statement in statements:
                            def_var = get_lvalue(statement.left)
                            d_dep = []
                            if not get_rhs_constants(statement.right):
                                get_rhs_cond_nodes(statement.right, d_dep)
                                for dep in d_dep:
                                    if dep in Params.keys():
                                        d_dep.remove(dep)
    
                            if def_var not in var_def_chain.keys():
                                var_def_chain[def_var] = {'CDeps': [c_dep],
                                                          'DDeps': [d_dep],
                                                          'Clocked': clocked,
                                                          'Expressions': [statement],
                                                          'CLines': [c_lines],
                                                          'DLines': [str(statement.lineno) + ':D']
                                                          }
                            else:
                                var_def_chain[def_var]['CDeps'].append(c_dep)
                                var_def_chain[def_var]['DDeps'].append(d_dep)
                                var_def_chain[def_var]['Expressions'].append(statement)
                                var_def_chain[def_var]['CLines'].append(c_lines),
                                var_def_chain[def_var]['DLines'].append(str(statement.lineno) + ':D')
                            del d_dep
                        del c_dep
                    cond.append(CDFG.get_edge_data(*edge)['cond'])
                    clines.append(str(CDFG.get_edge_data(*edge)['lineno']) + ':C' \
                            if CDFG.get_edge_data(*edge)['lineno'] else CDFG.get_edge_data(*edge)['lineno'])
                    #print cond
                #print '\n' * 2
        elif cdfg_type == 'Assign':
            ast = CDFG.node[CDFG.nodes()[0]]['ast']
            def_var = get_lvalue(ast.left)
            d_dep = []
            get_rhs_cond_nodes(ast.right, d_dep)
            if def_var not in var_def_chain.keys():
                var_def_chain[def_var] = {'CDeps': [],
                                          'DDeps': [d_dep],
                                          'Clocked': False,
                                          'Expressions': [ast],
                                          'CLines': [],
                                          'DLines': [str(ast.lineno) + ':D']
                                          }
            else:
                var_def_chain[def_var]['CDeps'].append([])
                var_def_chain[def_var]['DDeps'].append(d_dep)
                var_def_chain[def_var]['Expressions'].append(ast)
                var_def_chain[def_var]['CLines'].append([]),
                var_def_chain[def_var]['DLines'].append(str(ast.lineno) + ':D')

            del d_dep
    
    #pp.pprint(var_def_chain)

    return var_def_chain

def construct_var_dep_graph(var_def_chain, var_use_chain, mod_inst_chains):
    
    dep_g = nx.DiGraph()

    use_vars = var_use_chain.keys()

    for use_var in use_vars:
        # use_lines: a list of the line numbers of usage of the current use_vars that have been
        #            already analyzed. No need to analyze it and over constrain the graph
        use_lines = []
        # Add node to the dependency graph
        if use_var not in dep_g.nodes():
            dep_g.add_node(use_var)
        # Capturing Data Dependencies
        DefVars = var_use_chain[use_var]['DefVars']
        Lines = var_use_chain[use_var]['Lines']

        '''
        for DefVar in DefVars:
            if not DefVar:
                continue
            if DefVar not in dep_g.nodes():
                dep_g.add_node(DefVar)

            if not dep_g.has_edge(use_var, DefVar):
                # print 'Addine New Edge from: ' + use_var + ' to: ' + DefVar
                dep_g.add_edge(use_var, DefVar, weight=1.0)
            else:
                # print 'Increasing Edge weight from: ' + use_var + ' to: ' + DefVar
                dep_g[use_var][DefVar]['weight'] += 1.0
        '''
        # This has been done instead of the above commented code to avoid adding edges for the same
        # usage of a variable to define another variable reached via a different path. Adding weight for
        # the same definition over-constraints the variable dependency graph
        for idx1 in range(len(DefVars)):
            DefVar = DefVars[idx1]
            Line = Lines[idx1]

            if not DefVar:
                continue
            if DefVar not in dep_g.nodes():
                dep_g.add_node(DefVar)

            if not dep_g.has_edge(use_var, DefVar):
                dep_g.add_edge(use_var, DefVar, weight=1.0)
                use_lines.append(Line)
            else:
                if Line not in use_lines:
                    dep_g[use_var][DefVar]['weight'] += 1.0

    def_vars = var_def_chain.keys()

    for def_var in def_vars:
        # Add node to the dependency graph
        if def_var not in dep_g.nodes():
            dep_g.add_node(def_var)

        CDeps = var_def_chain[def_var]['CDeps']
        for CDep in CDeps:
            CDep_flatened = list(itools.chain(*CDep))
            for ele in CDep_flatened:
                if not ele:
                    continue
                if ele not in dep_g.nodes():
                    dep_g.add_node(ele)

                if not dep_g.has_edge(ele, def_var):
                    dep_g.add_edge(ele, def_var, weight=1.0)
                else:
                    dep_g[ele][def_var]['weight'] += 1.0
    
    '''
    for mod_inst_chain in mod_inst_chains:
        root_module = get_root_node(mod_inst_chain)[0]

        instance_modules = mod_inst_chain.nodes()
        instance_modules.remove(root_module)
        
        if not instance_modules:
            continue
        
        print instance_modules
        for instance in instance_modules:
            portname = mod_inst_chain.node[instance]['portname']
            argname = mod_inst_chain.node[instance]['argname']
            module_name = mod_inst_chain.node[instance]['module_name']

            mports, mtyps = get_port_type(module_name, mod_inst_chains)

            for idx in range(len(portname)):
                #if portname[idx] == argname[idx]:
                #    continue
                p_index = mports.index(portname[idx])
                typ = mtyps[p_index]

                if typ == 'inp':
                    for arg in argname[idx]:
                        if arg != portname[idx]:
                            if not dep_g.has_edge(arg, portname[idx]):
                                dep_g.add_edge(arg, portname[idx], weight=1.0)
                elif typ == 'oup':
                    for arg in argname[idx]:
                        if arg != portname[idx]:
                            if not dep_g.has_edge(portname[idx], arg):
                                dep_g.add_edge(portname[idx], arg, weight=1.0)
    '''
    

    return dep_g

def cone_of_influence(vtargets, var_def_chain, var_use_chain, \
                      dep_g, PageRank, temp_length_max):
    # vtargets: list of target variables identified. Primarily it contains the primary outputs and
    #           register variables

    # dep_g: variable dependency graph

    # Basic depth first search has beene employed

    # COI:  a directed graph where the root node is the target variable and each leaf node is the variable
    #       on which target variable depends on

    cones = {}

    for vtarget in vtargets:
        cone = nx.DiGraph()
        target_importance = PageRank[vtarget]
        target_complexity = tcomplexity(vtarget, var_def_chain, var_use_chain)
        cone.add_node(vtarget, importance=target_importance,\
                               complexity=target_complexity, \
                               rank = 0.0 # We initialize rank of the target node
                               )
        temporal_cone(vtarget, 0, temp_length_max, vtarget, PageRank, dep_g, \
                var_def_chain, var_use_chain, cone)
        #print coi.nodes()
        for n in cone.nodes():
            try:
                cone.node[n]['rank'] = cone.node[n]['importance'] / cone.node[n]['complexity']
            except ZeroDivisionError:
                cone.node[n]['rank'] = 0.0

        cones[vtarget] = cone
        #print cone.nodes() 
        #print nx.get_node_attributes(cone, 'importance')
        #print nx.get_node_attributes(cone, 'complexity')
        #print nx.get_node_attributes(cone, 'rank')
    return cones

def temporal_cone(var, temp_length, temp_length_max, vtarget, PageRank, dep_g, \
        var_def_chain, var_use_chain, cone):

    # helper function for cone_of_influence

    if temp_length < temp_length_max:
        # TODO: import dependencies / move it from assertion_analyzer.py
        V = dependencies(var, var_def_chain, dep_g, temp_length, temp_length_max)
        # TODO: import expressions / move it from assertion_analyzer.py
        var_name = var[var.find(']') + 1:]
        X = expressions(var_name, var_def_chain)
        X_Keys = X.keys()

        for v in V:

            # Done to remove the time stamp from the true var name
            v_name = v[v.find(']') + 1:]
            # Relative Importance Calculation Part
            Ig_v = PageRank[v_name]
            Ir_vtarget = cone.node[var]['importance']
            v_vtarget_weight = dep_g.get_edge_data(v_name, var_name)['weight']
            Ir_v_vtarget = Ig_v + v_vtarget_weight * Ir_vtarget

            # Relative Complexity Calculation Part. 
            # TODO: import sensitivities / move it from assertion_analyzer.py
            S = sensitivities(v_name, var_use_chain)
            S_Keys = S.keys()
            Cr_v_target = 0
            Comm_Expr_Keys = list(set(X_Keys).intersection(set(S_Keys)))
            for key in Comm_Expr_Keys:
                Comm_Expr = X[key]
                Cr_v_target = Cr_v_target + expr_length(Comm_Expr)

            # On my hunch
            Cr_v_target = Cr_v_target + cone.node[var]['complexity']

            # Add the variable nodes with all the information
            # Node_Name = '[' + str(temp_length) + ']' + v if temp_length > 0 else v
            # cone.add_node(Node_Name, importance=Ir_v_vtarget, \
            cone.add_node(v, importance=Ir_v_vtarget, \
                                     complexity=Cr_v_target, \
                                     rank=0.0)
            # cone.add_edge(Node_Name, var)
            cone.add_edge(v, var)

        for v in V:
            # TODO: import temporal / move it from assertion_analyzer.py
            v_name = v[v.find(']') + 1:]
            if temporal(var_name, var_def_chain):
                # v = '[' + str(temp_length) + ']' + v if temp_length > 0 else v
                temporal_cone(v, temp_length + 1, temp_length_max, vtarget, PageRank, dep_g, \
                        var_def_chain, var_use_chain, cone)
            else:
                # v = '[' + str(temp_length) + ']' + v if temp_length > 0 else v
                temporal_cone(v, temp_length, temp_length_max, vtarget, PageRank, dep_g, \
                        var_def_chain, var_use_chain, cone)

    return

def find(dep, DDeps):
    for i, Dep in enumerate(DDeps):
        try:
            j = Dep.index(dep)
        except ValueError:
            continue
        return (i, j)

    return (None, None)

def dependencies(var, var_def_chain, dep_g, k, kmax):
    
    deps = []
    #print 'From dependencies: ' + var
    var_name = var[var.find(']') + 1:]
    deps_from_dep_g = dep_g.predecessors(var_name)
    #print 'From dependencies: ' + str(deps_from_dep_g)

    if not deps_from_dep_g:
        # var is a primary input and has no dependencies
        return deps

    DDeps = var_def_chain[var_name]['DDeps']
    Clocked = var_def_chain[var_name]['Clocked']
    #print 'From dependencies: ' + str(DDeps)
    Expressions = var_def_chain[var_name]['Expressions']
    #print 'From dependencies: ' + str(Expressions)
    
    for dep in deps_from_dep_g:
        found = find(dep, DDeps)
        #print 'From dependencies: ' + str(found)

        ## Handling data dependencies ##
        if found[0] != None:
            expression = Expressions[found[0]]
            #print 'Fromd dependencies: ' + str(expression)
            exp_typ = expression.__class__.__name__
            #print exp_typ
            if exp_typ == 'NonblockingSubstitution':
                nxt_k = k + 1
                if nxt_k < kmax:
                    lookback_string = '[' + str(nxt_k) + ']' + dep
                    deps.append(lookback_string)
                    #print 'Fromd dependencies: ' + lookback_string
            else:
                lookback_string = '[' + str(k) + ']' + dep if k > 0 else dep
                deps.append(lookback_string)
        
        ## Handling control dependencies ##
        else:
            if Clocked:
                nxt_k = k + 1
                if nxt_k < kmax:
                    lookback_string = '[' + str(nxt_k) + ']' + dep
                    deps.append(lookback_string)
            else:
                lookback_string = '[' + str(k) + ']' + dep if k > 0 else dep
                deps.append(lookback_string)

    #print 'From dependencies: ' + str(deps)

    del deps_from_dep_g

    #print '\n' * 2

    return deps

def tcomplexity(var, var_def_chain, var_use_chain):

    complexity = 0

    X = expressions(var, var_def_chain)
    X_Keys = X.keys()

    S = sensitivities(var, var_use_chain)
    S_Keys = S.keys()
    Comm_Expr_Keys = list(set(X_Keys).intersection(set(S_Keys)))
    for key in Comm_Expr_Keys:
        Comm_Expr = X[key]
        complexity = complexity + expr_length(Comm_Expr)

    return complexity

def expr_length(Comm_Expr):
    
    Length = 0

    if type(Comm_Expr) is list:
        Length = len(Comm_Expr)
    else:
        Expr_Var = []
        get_rhs_cond_nodes(Comm_Expr, Expr_Var)
        Length = len(Expr_Var)

    return Length
        

def temporal(v, var_def_chain):
    def_lists = []

    try:
        def_lists = var_def_chain[v]['Expressions']
    except KeyError:
        return False

    for defn in def_lists:
        if defn.__class__.__name__ == 'NonblockingSubstitution':
            return True

    return False

def dependencies_deprecated(v, dep_g):
    # This returns the immediate predecessors in the dep_g graph. But that does not associate the 
    # time stamp of the dependent variable. Hence this is too crude and wont worl. Writing a new dependencies
    # above. Renaming it as dependencies_deprecated
    return dep_g.predecessors(v)

def expressions(v, var_def_chain):
    # FIXME: line no for control statements
    X = {}
    try:
        cdeps = var_def_chain[v]['CDeps']
        clines = var_def_chain[v]['CLines']
        expression = var_def_chain[v]['Expressions']
        dlines = var_def_chain[v]['DLines']
    except KeyError:
        return X
    
    # Adding data dependency expressions
    for idx in range(len(expression)):
        expr = expression[idx]
        lineno = dlines[idx]
        X[lineno] = expr

    # Adding control dependency expressions
    for idx in range(len(cdeps)):
        cdep = cdeps[idx]
        cline = clines[idx]
        for i in range(len(cdep)):
            if cdep[i]:
                expr = cdep[i]
                lineno = cline[i]
                X[lineno] = expr
    return X

def sensitivities(v, var_use_chain):
    S = {}

    try:
        sensitivity = var_use_chain[v]['Sensitivities']
        line = var_use_chain[v]['Lines']
    except KeyError:
        return S
    
    for idx in range(len(sensitivity)):
        sens = sensitivity[idx]
        lineno= line[idx]
        S[lineno] = sens

    return S


def process_targets(cones, Ports, targets, vtargets):
    
    signals = {}
    
    signals.update(Ports['IPort'])
    signals.update(Ports['OPort'])
    signals.update(Ports['Reg'])
    signals.update(Ports['Wire'])

    target_cones = {}

    signals_keys = signals.keys()
    cone_keys = cones.keys()
    
    sig_name_pattern = re.compile(r'(\[[0-9]*\])?([A-Za-z0-9_]+)')

    for ckey in cone_keys:
        if ckey in signals_keys:
            # only for bit level target signal
            if signals[ckey] == 1:
                cnodes = cones[ckey].nodes()
                cnodes.remove(ckey)
                target_cones[ckey] = []
                for node in cnodes:
                    match_s = re.search(sig_name_pattern, node)
                    if match_s:
                        signal_name = match_s.group(2)
                        try:
                            if signals[signal_name] == 1:
                                target_cones[ckey].append(node)
                            else:
                                nodes = [node + '[' + str(i) + ']' for i in range(signals[signal_name])]
                                target_cones[ckey].extend(nodes)
                                del nodes
                        except KeyError:
                            target_cones[ckey] = []
                            break
            # Still need to process whne the targets are the bus signals

    for ckey in target_cones.keys():
        if not target_cones[ckey]:
            try:
                del target_cones[ckey]
            except KeyError:
                pass

    return target_cones

def get_targets_of_manual_assertions(massertions):
    mtargets = {}

    for ele in massertions:
        mtarget = ele[-1][0]
        if mtarget not in mtargets.keys():
            mtargets[mtarget] = [ele]
        else:
            mtargets[mtarget].append(ele)


    return mtargets
'''
def find_COI(dep_g, node, coi, p_node):

    if node not in coi.nodes():
        coi.add_node(node)
        if p_node:
            coi.add_edge(p_node, node)

        for n in dep_g.predecessors(node):
            find_COI(dep_g, n, coi, node)

    return
'''
