# GoldMine Re-Implementation Code in Python
# Authors: Debjit Pal ( dpal2@illinois.edu )
# Authors: Spencer Offenberger ( so10@illinois.edu )
# Owner:   Shobha Vasudevan ( shobhav@illinois.edu )

########### BUILD COMMAND #############
# python setup.py build_ext --inplace #
########### BUILD COMMAND #############


from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize

setup(
        name = 'VCD Parse TimeFrames App',
        version = '0.1',
        ext_modules = cythonize('parse_timeframes.pyx'),
        )
